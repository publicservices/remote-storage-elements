import Widget from 'https://cdn.skypack.dev/remotestorage-widget'

const RsWidget = class extends HTMLElement {
    constructor() {
	super()
    }
    connectedCallback() {
	this.getAttributes()
	this.setupWidget()
    }

    getAttributes() {
	this.debug = this.getAttribute('debug') ? true : false
	this.dettach = this.getAttribute('dettach') ? true : false
	
	this.elDomId = 'rs-widget-' + Date.now()
	this.setAttribute('id', this.elDomId)

	this.remoteStorageDOM = document.querySelector('rs-storage')	

	if (!this.remoteStorageDOM) {
	    console.error('Missing <rs-storage> element')
	    return
	} 

	this.remoteStorage = this.remoteStorageDOM.remoteStorage
	/* use if race condition */
	/* if (!this.remoteStorage) {
	   this.remoteStorageDOM
	   .addEventListener(
	   'remoteStorageConnected',
	   this.handleConnected,
	   false
	   )
	   } */
    }
    
    setupWidget() {
	this.remoteWidget = new Widget(this.remoteStorage, {
	    skipInitial: true
	})

	if (this.dettach) {
	    this.remoteWidget.attach()
	} else {
	    this.remoteWidget.attach(this.elDomId)
	}

	if (this.debug) {
	    console.log('remoteWidget', this.remoteWidget)
	}
    }
}

export default RsWidget
